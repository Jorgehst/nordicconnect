import sys
import time
import logging
from queue import Queue, Empty
from pc_ble_driver_py.observers import *
import os.path
mac_dev = "00A05028328A"
TARGET_DEV_NAME = "SmartInsole"
#CONNECTIONS = 2
CFG_TAG = 1
dataList = []


def init(conn_ic_id):
    # noinspection PyGlobalUndefined
    global config, BLEDriver, BLEAdvData, BLEEvtID, BLEAdapter, BLEEnableParams, BLEGapTimeoutSrc, BLEUUID, BLEConfigCommon, BLEConfig, BLEConfigConnGatt, BLEGapScanParams
    from pc_ble_driver_py import config

    config.__conn_ic_id__ = conn_ic_id
    # noinspection PyUnresolvedReferences
    from pc_ble_driver_py.ble_driver import (
        BLEDriver,
        BLEAdvData,
        BLEEvtID,
        BLEEnableParams,
        BLEGapTimeoutSrc,
        BLEUUID,
        BLEGapScanParams,
        BLEConfigCommon,
        BLEConfig,
        BLEConfigConnGatt,
    )

    # noinspection PyUnresolvedReferences
    from pc_ble_driver_py.ble_adapter import BLEAdapter

    global nrf_sd_ble_api_ver
    nrf_sd_ble_api_ver = config.sd_api_ver_get()

#Configuracion para NRF52

class HRCollector(BLEDriverObserver, BLEAdapterObserver):
    def __init__(self, adapter):
        super(HRCollector, self).__init__()
        self.adapter = adapter
        self.conn_q = Queue()
        self.adapter.observer_register(self)
        self.adapter.driver.observer_register(self)
        self.adapter.default_mtu = 250

    def open(self):
        self.adapter.driver.open()
        
        gatt_cfg = BLEConfigConnGatt()
        gatt_cfg.att_mtu = self.adapter.default_mtu
        gatt_cfg.tag = CFG_TAG
        self.adapter.driver.ble_cfg_set(BLEConfig.conn_gatt, gatt_cfg)

        self.adapter.driver.ble_enable()
        BLEEnableParams(
            vs_uuid_count=1,
            service_changed=0,
            periph_conn_count=0,
            central_conn_count=2,
            central_sec_count=0,
        )

    def close(self):
        self.adapter.driver.close()

    #Descrubre y conecta al mismo tiempo
    def connect_and_discover(self):
        scan_duration = 8
        params = BLEGapScanParams(interval_ms=200, window_ms=150, timeout_s=scan_duration)

        self.adapter.driver.ble_gap_scan_start(scan_params=params)

        try:
            new_conn = self.conn_q.get(timeout=scan_duration)
            self.adapter.service_discovery(new_conn)
            #self.adapter.driver.ble_vs_uuid_add(BASE_UUID)
            #print(self.adapter.db_conns)
            for s in self.adapter.db_conns[new_conn].services:
                print(s)
                #printea caracteristicas disponibles
                for c in s.chars:
                    print(c)
            #UUID de la caracteristica a leer
            chosenValue = 0x2A53 #int(input("Inserte UUID de la característica deseada: "), 16) #0x2A53

            for s in self.adapter.db_conns[new_conn].services:
                for c in s.chars :                    
                    if s.uuid.value == chosenValue: #and c.uuid.value == 0x2A19:
                         # print(c.uuid)
                         #habilita la notificacion de la uuid elegida
                         self.adapter.enable_notification(
                             new_conn, c.uuid
                         )
                         

                        #status2, data2 = self.adapter.read_req(new_conn,c.uuid)
                        #print('Info = {} status = {}'.format(data2,status2))
                        
            """
           
            """
            #status, data = self.adapter.read_req(new_conn,BLEUUID(BLEUUID.Standard.battery_level))
            #print('Battery level read value = {} status = {}'.format(data,status))
            

            return new_conn
        except Empty:
            print(f"No heart rate collector advertising with name {TARGET_DEV_NAME} found.")
            return None

    def char_handler(self, **kwargs):
        print(kwargs)
        return "HolaMundo!"


    def on_gap_evt_connected(
        self, ble_driver, conn_handle, peer_addr, role, conn_params
    ):
        print("New connection: {}".format(conn_handle))
        self.conn_q.put(conn_handle)

    def on_gap_evt_disconnected(self, ble_driver, conn_handle, reason):
        print("Disconnected: {} {}".format(conn_handle, reason))

    def on_gap_evt_adv_report(
        self, ble_driver, conn_handle, peer_addr, rssi, adv_type, adv_data
    ):
        if BLEAdvData.Types.complete_local_name in adv_data.records:
            dev_name_list = adv_data.records[BLEAdvData.Types.complete_local_name]

        elif BLEAdvData.Types.short_local_name in adv_data.records:
            dev_name_list = adv_data.records[BLEAdvData.Types.short_local_name]

        else:
            return

        dev_name = "".join(chr(e) for e in dev_name_list)
        address_string = "".join("{0:02X}".format(b) for b in peer_addr.addr)
        print(
            "Received advertisment report, address: 0x{}, device_name: {}".format(
                address_string, dev_name
            )
        )
        #print("adress_string: " + address_string)
        #print("--------------------------------------------------------------------")
        #print("direccion" + str(peer_addr))

        #Conecta con la mac
        if (address_string == mac_dev): #and """(dev_name == TARGET_DEV_NAME):
            self.adapter.connect(peer_addr, tag=CFG_TAG)
            print(type(peer_addr))
            

    def on_notification(self, ble_adapter, conn_handle, uuid, data):
        if len(data) > 32:
            data = "({}...)".format(data[0:10])
        print("Connection: {}, {} = {}".format(conn_handle, uuid, data))
        dataList.append(data) #Insert all data into a list
        print("hey")
        print(dataList)

    def on_gattc_evt_read_rsp(self, ble_driver, conn_handle, status, error_handle, attr_handle, offset,data):
        print('Read Response event - Connection: {}, {} = {}'.format(conn_handle, attr_handle, data))

    def on_att_mtu_exchanged(self, ble_driver, conn_handle, att_mtu):
        print('ATT MTU exchanged: conn_handle={} att_mtu={}'.format(conn_handle, att_mtu))

    def on_gattc_evt_exchange_mtu_rsp(self, ble_driver, conn_handle, **kwargs):
        print('ATT MTU exchange response: conn_handle={}'.format(conn_handle))


def main(selected_serial_port):
    print("Serial port used: {}".format(selected_serial_port))
    driver = BLEDriver(
        serial_port=selected_serial_port, auto_flash=False, baud_rate=1000000, log_severity_level="info"
    )

    adapter = BLEAdapter(driver)
    collector = HRCollector(adapter)
    collector.open()
    conn = collector.connect_and_discover()

    if conn is not None:
        time.sleep(10)

    collector.close()



if __name__ == "__main__":
    init("NRF52")
    main("COM5")

    """
    if len(sys.argv) == 3:
        init(sys.argv[1])
        main(sys.argv[2])
    else:
        print("Invalid arguments. Parameters: <conn_ic_id> <serial_port>")
        print("conn_ic_id: NRF51, NRF52")
    """
    quit()